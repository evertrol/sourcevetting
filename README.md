Sourcevetting is a Django application to easily vet soures found in
difference imaging.


Installation
------------

You can copy the sourcevetting subdirectory into your Django
application, or simply run

```
python setup.py install
```

and then add the 'sourcevetting' app to your INSTALLED_APPS in your
Django project settings.


Dependencies
------------

- Django
- AplPy
- Matplotlib

The main dependency is Django. AplPy and Matplotlib are only used in
the 'storeobs' manager command.


Manager command
---------------

The app adds a manager command, 'storeobs'. This will take an
appropriate FITS file, extract the necessary data from it and store it
in database. It will also create images and store these in an
appropriate subdirectory of the 'media/' directory. Finally, it copies
the input FITS file to this subdirectory.

Use in a project
----------------

The app relies on a few conventions to be able to style its page properly:

- all HTML templates derive from a "base.html" template.

- this base template should have the following blocks:

    - stylesheets: CSS and other stylesheets will appear inside this block

    - scripts: ECMAscript and other scripts will appear inside this block

    - content: the actual contents appears inside this block.
