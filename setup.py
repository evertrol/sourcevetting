#! /usr/bin/env python
"""Installation script"""

from setuptools import setup


setup(
    name='sourcevetting',
    version='alpha',
    description='GOTO source vetting web-app',
    license='FreeBSD',
    copyright='GOTO Observatory',
    author='Evert Rol',
    author_email='evert.rol@monash.nl',
    url='https://github.com/GOTO-OBS',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: FreeBSD License',
        'Programming Language :: Python',
    ],
    packages=['sourcevetting', 'sourcevetting.migrations',
              'sourcevetting.management',
              'sourcevetting.management.commands'],
    package_dir={'sourcevetting': 'sourcevetting'},
    include_package_data=True,
    install_requires=['django', 'aplpy', 'matplotlib']
)
