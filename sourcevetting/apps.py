from django.apps import AppConfig


class VettingConfig(AppConfig):
    name = 'sourcevetting'
