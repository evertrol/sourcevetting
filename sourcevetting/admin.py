from django.contrib import admin
from .models import Target, Night, Observation, ObsField, Vote, Source


@admin.register(Target)
class TargetAdmin(admin.ModelAdmin):
    pass


@admin.register(Night)
class NightAdmin(admin.ModelAdmin):
    pass


@admin.register(Observation)
class ObservationAdmin(admin.ModelAdmin):
    pass


@admin.register(ObsField)
class ObsFieldAdmin(admin.ModelAdmin):
    pass


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    pass


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    pass
