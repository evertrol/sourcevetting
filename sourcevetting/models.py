from django.db import models
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from django.utils.html import format_html
from datetime import timedelta


__all__ = ['Target', 'Night', 'Observation', 'ObsField', 'Vote', 'Source']


def fieldpath(instance, filename):
    # file are uploaded to MEDIA_ROOT/subdir/filename
    subdir = os.path.join('fields', instance.name)
    return '{}/{}'.format(subdir, filename)


class Position(models.Model):
    ra = models.FloatField()
    dec = models.FloatField()

    def __str__(self):
        return "{:.3f}, {:.3f}".format(self.ra, self.dec)

    def degrees(self):
        return str(self)

    def hmsdms(self):
        from astropy.coordinates import SkyCoord
        c = SkyCoord(ra=self.ra, dec=self.dec, unit='degree')
        return c.to_string('hmsdms')


class Target(models.Model):
    """Target of interest"""
    name = models.CharField(max_length=255, unique=True)
    center = models.ForeignKey(Position, blank=True, null=True)

    def __str__(self):
        return self.name


class Magnitude(models.Model):
    """Magnitude value, with error in a given filter"""
    filt = models.CharField(max_length=40)
    value = models.FloatField()
    error = models.FloatField()

    def __str__(self):
        return "{:.2f} +/- {:.2f} ({})".format(self.value, self.error,
                                               self.filt)

    def __html__(self):
        return "{:.2f} &plusmn; {:.2f} ({})".format(self.value, self.error,
                                                    self.filt)


class Night(models.Model):
    """Class to hold all observations on a given night.

    The night is defined by the local date at the start of the night

    """
    startdate = models.DateField()


class Observation(models.Model):
    center = models.OneToOneField(Position, blank=True, null=True)
    night = models.ForeignKey(Night)

#    def date(self, localnoon=12):
#        """Return the date for the *start* of the observing run
#
#        localnoon gives the UTC hour for local midday
#        """
#        delta = timedelta(1)
#        observations = self.obsfield_set()
#        dates = [observation.date for observation in observations]
#        dates = [date.date() if date.hour > localnoon else date.date() - delta
#                 for date in dates]
#        return min(dates)


class ObsField(models.Model):
    """Single camera field

    Individual cameras are tied together through the Observation
    model. The Observations model doesn't have many fields in common
    though, because
    - a camera may start late or finish early, thus having different
      date & exptime;
    - different filters may apply to each camera

    Only the overall pointing center and date of the starting night is
    between ObsFields.

    """
    name = models.CharField(max_length=80)
    target = models.ForeignKey(Target)
    observation = models.ForeignKey(Observation, blank=True, null=True)

    centerpos = models.ForeignKey(Position)
    date = models.DateTimeField()
    datestart = models.DateTimeField()
    dateend = models.DateTimeField()
    limmag = models.ForeignKey(Magnitude)
    exptime = models.FloatField()
    filt = models.CharField(max_length=80)
    camera = models.CharField(max_length=80)

    image = models.FileField(upload_to=fieldpath, max_length=1024)
    fitsfile = models.FileField(upload_to=fieldpath, max_length=1024)

    def __str__(self):
        return self.name

    def sources(self):
        return self.source_set.order_by('-validity').all()


class Feature(models.Model):
    name = models.CharField(max_length=80)
    value = models.FloatField()
    source = models.ForeignKey('Source')


class Vote(models.Model):
    voted = models.BooleanField(default=False)
    value = models.PositiveSmallIntegerField()
    user = models.ForeignKey(User, related_name='+')


class Source(models.Model):

    vote1 = models.ManyToManyField('Vote', blank=True,
                                   related_name='vote1sources')
    vote2 = models.ManyToManyField('Vote', blank=True,
                                   related_name='vote2sources')
    vote3 = models.ManyToManyField('Vote', blank=True,
                                   related_name='vote3sources')

    detidx = models.IntegerField()

    cls = models.CharField(max_length=80,
                           help_text="Suggested source classification")
    conf = models.FloatField(help_text="Confidence of source classification")
    validity = models.FloatField(help_text="Validity of source "
                                 "being a transient")

    diffthumb = models.FileField(upload_to=fieldpath)
    detthumb = models.FileField(upload_to=fieldpath)
    srcthumb = models.FileField(upload_to=fieldpath)
    refthumb = models.FileField(upload_to=fieldpath)

    field = models.ForeignKey(ObsField)

    pos = models.OneToOneField(Position)


    def nfeatures(self):
        return self.feature_set.count()

    def features(self):
        return self.feature_set.order_by('name')

    def render_vote1(self):
        return format_html('<input type="checkbox">')

    def render_vote2(self):
        return format_html("""\
<ul class="voting">
<li><label class="vote"><input type="radio" name="{index}" value="yes">yes</label></li>
<li><label class="vote"><input type="radio" name="{index}" value="none" checked="checked">&mdash;</label></li>
<li><label class="vote"><input type="radio" name="{index}" value="no">no</label></li>
</ul>
""".format(index=self.pk))

    def render_vote3(self):
        return format_html('<input type="range" min="1" max="5" value="3" list="votelist">')
