from django.conf.urls import include, url
from .views import *


urlpatterns = [
    url(r'^field/(?P<field>\d+)/source/(?P<pk>\d+)/$',
        SourceView.as_view(),
        name='source'),
    url(r'^field/(?P<pk>\d+)/$',
        FieldView.as_view(),
        name='field'),
    url(r'^field/$',
        FieldsView.as_view(),
        name='fields'),
    url(r'^night/(?P<startdate>[-\d]+)/$',
        NightView.as_view(),
        name='night'),
    url(r'^night/$',
        NightsView.as_view(),
        name='nights'),
    url(r'^(?P<pk>\d+)/$',
        TargetView.as_view(),
        name='target'),
    url(r'^$',
        IndexView.as_view(),
        name='index'),
]
