from django.views.generic import TemplateView, DetailView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
import datetime
from .models import *



class SourceView(LoginRequiredMixin, TemplateView):
    template_name = 'sourcevetting/index.html'


class FieldView(LoginRequiredMixin, DetailView):
    model = ObsField
    context_object_name = 'field'
    #template_name = 'sourcevetting/index.html'


class FieldsView(LoginRequiredMixin, ListView):
    model = ObsField
    ordering = ['date', 'pk']


class TargetView(LoginRequiredMixin, ListView):
    model = ObsField
    ordering = 'date'

    def get_queryset(self):
        target = Target.objects.get(pk=self.kwargs['pk'])
        return target.obsfield_set.all()


class IndexView(LoginRequiredMixin, ListView):
    model = Target


class NightView(LoginRequiredMixin, DetailView):
    model = Night

    def get_object(self):
        date = self.kwargs['startdate']
        return self.model.objects.filter(startdate=date)


class NightsView(LoginRequiredMixin, ListView):
    #template_name = 'sourcevetting/dates.html'
    model = Night
    ordering = 'startdate'
