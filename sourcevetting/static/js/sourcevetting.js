$(function() {
	var $table = $('table.floatThead');
	$table.floatThead();

	$('#select-vote-style').change(function () {
		var el = document.getElementById('select-vote-style');
		var nr = parseInt(el.options[el.selectedIndex].value);
		for (var i = 1; i < 4; i++) {
			var key = '.vote' + i;
			if (i === nr) {
				$(key).css('display', 'table-cell');
			} else {
				$(key).css('display', 'none');
			}
		}
	});
	
});

function popup_table(elid) {
	var popup = document.getElementById(elid);
	popup.classList.toggle("hide");
}
