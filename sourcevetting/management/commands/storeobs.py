import os
import shutil
import re
import subprocess
import logging
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from PIL import Image
import aplpy
from astropy import units
from astropy.io import fits
from astropy.time import Time
from astropy.table import Table
from astropy.visualization import ZScaleInterval
from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from django.conf import settings
from django.db import transaction
from ...models import Position, Target, Magnitude, ObsField, Source, Feature
try:  # no need for six
    basestring   # Python 2
    # exist_ok is not available in Python 2
    def makedirs(name, mode=0o777, exist_ok=False):
        try:
            os.makedirs(name, mode)
        except OSError:
            if not exist_ok:
                raise
    # shutil.copy does not return the new filename in Python 2
    def filecopy(src, dst):
        shutil.copy(src, dst)
        return os.path.join(dst, os.path.basename(src))
except NameError:  # Python 3
    makedirs = os.makedirs
    filecopy = shutil.copy



def create_fieldimage(hdulist, dirname, nmax=None):
    logger = logging.getLogger(__name__)
    logger.info("Creating PNG field image")
    hdu = hdulist['difference']
    figure = aplpy.FITSFigure(data=hdu, figsize=(32,32))

    figure.show_grayscale()
    figure.add_grid()
    figure.grid.set_xspacing(0.5)
    figure.grid.set_yspacing(0.5)
    figure.grid.set_alpha(0.6)
    figure.grid.set_color('green')
    figure.grid.set_linestyle('dashed')
    figure.add_scalebar(1 * units.deg)
    figure.scalebar.set_label('1 degree')

    dettable = Table.read(hdulist['detections'])
    ra = dettable['ra']
    dec = dettable['dec']
    if nmax is not None:
        scores = Table.read(hdulist['scores'])
        indices = np.argsort(scores['validity'])[::-1]
        indices = indices[scores['validity'] > 0.1]
        indices = indices[:nmax]
    else:
        indices = np.arange(len(dettable), dtype=np.int)
    #figure.show_circles(ra, dec, 0.02, edgecolor='red')
    for i in indices:
        figure.add_label(ra[i], dec[i], str(i), relative=False,
                         ha='left', va='bottom', size=6, color='cyan')

    figure._figure.tight_layout()

    filename = os.path.join(dirname, 'field-overview.png')
    canvas = FigureCanvas(figure._figure)
    canvas.print_figure(filename, dpi=150)
    return filename


def store_field(hdulist, fieldname, targetname, nmax=None):
    logger = logging.getLogger(__name__)
    target, _ = Target.objects.get_or_create(name=targetname)

    header = hdulist['photometry'].header
    limmag = Magnitude(filt='V', value=header['limmag_ideal_apass_v'],
                       error=header['limmerr_ideal_apass_v'])
    limmag.save()

    if hdulist[0].data is None or hdulist[0].data.ndim != 2:
        header = hdulist['image'].header
    else:
        header = hdulist[0].header
    ra = header['cra']
    dec = header['cdec']
    pos = Position(ra=ra, dec=dec)
    pos.save()

    exptime = header['exptime']
    date = Time(header['date-obs']).datetime
    datestart = Time(header['date-obs']).datetime
    dateend = Time(header['date-obs'])
    dateend = dateend + exptime * units.second
    dateend = dateend.datetime
    filt = header['filter']
    camera = str(header['instrume'])

    field = ObsField(name=fieldname, target=target, centerpos=pos,
                     limmag=limmag,
                     date=date, datestart=datestart, dateend=dateend,
                     exptime=exptime, filt=filt, camera=camera,
                     image="none", fitsfile="none")
    field.save()

    subdir = os.path.join(settings.MEDIA_ROOT, 'sourcevetting',
                          'fields', fieldname, str(field.pk))
    makedirs(subdir, exist_ok=True)
    pngimage = create_fieldimage(hdulist, subdir, nmax)
    logger.debug("Copying FITS image")
    fitsfile = filecopy(hdulist.filename(), subdir)
    if pngimage.startswith(settings.MEDIA_ROOT):
        pngimage = pngimage[len(settings.MEDIA_ROOT):].lstrip('/')
    if fitsfile.startswith(settings.MEDIA_ROOT):
        fitsfile = fitsfile[len(settings.MEDIA_ROOT):].lstrip('/')
    field.image = pngimage
    field.fitsfile = fitsfile
    field.save()

    return field


def create_thumbnail(image, name, index, field):
    logger = logging.getLogger(__name__)
    logger.debug("z-scaling data")
    vmin, vmax = ZScaleInterval().get_limits(image)
    logger.debug("clipping between %f and %f", vmin, vmax)
    image = (np.clip(image, vmin, vmax) - vmin) / (vmax - vmin)
    image = (255 * image).astype(np.uint8)
    # enlarge for easier viewing
    # don't interpolate pixels; repeat them instead
    logger.debug("enlarging image")
    image = np.repeat(np.repeat(image, 10, axis=1), 10, axis=0)
    logger.debug("creating PIL image")
    image = Image.fromarray(image).transpose(Image.FLIP_TOP_BOTTOM)
    subdir = os.path.join(settings.MEDIA_ROOT, 'sourcevetting',
                          'fields', field.name, str(field.pk))
    filename = os.path.join(subdir, '{}-{}.png'.format(name, index))
    logger.debug("saving image")
    image.save(filename)
    logger.debug("saved")
    return filename[len(settings.MEDIA_ROOT):].lstrip('/')


@transaction.atomic
def store_features(features, source):
    logger = logging.getLogger(__name__)
    # FloatField does not take NaN/Infinity
    logger.debug("Storing features: %s", features.dtype.names)
    features = [Feature(name=name, value=features[name], source=source)
                for name in features.dtype.names]
    Feature.objects.bulk_create(features)
    #for name in features.dtype.names:
    #    #if name == 'photid':
    #    #    continue
    #    feature = Feature(name=name, value=features[name], source=source)
    #    feature.save()
    logger.debug("features stored")

def store_sources(hdulist, field, nmax=None):
    logger = logging.getLogger(__name__)
    logger.info("Reading source table from FITS file")
    scores = Table.read(hdulist['scores'])
    features = Table.read(hdulist['features'])
    indices = np.argsort(scores['validity'])[::-1]
    indices = indices[scores['validity'][indices] > 0.1]
    if nmax:
        indices = indices[:nmax]

    # Reduce the data size
    scores = scores[indices]
    features = Table.read(hdulist['features'])[indices]
    # nan_to_num does not work on recarrays as a whole
    for name in features.dtype.names:
        features[name] = np.nan_to_num(features[name])
    logger.info("Available features: %s", features.dtype.names)

    detections = Table.read(hdulist['detections'])[indices]
    srcthumbs = hdulist['thumbnails'].data
    srcthumbs = srcthumbs[indices,...]
    diffthumbs = hdulist['difference_thumbnails'].data
    diffthumbs = diffthumbs[indices,...]
    detthumbs = hdulist['detectimg_thumbnails'].data
    detthumbs = detthumbs[indices,...]
    refthumbs = hdulist['reference_thumbnails'].data
    refthumbs = refthumbs[indices,...]

    logger.info("Creating source thumbnails")
    for i, index in enumerate(indices):
        validity = scores['validity'][i]
        ra = detections[i]['ra']
        dec = detections[i]['dec']
        pos = Position(ra=ra, dec=dec)
        pos.save()
        diffthumb = create_thumbnail(diffthumbs[i], 'diff', index, field)
        detthumb = create_thumbnail(detthumbs[i], 'det', index, field)
        srcthumb = create_thumbnail(srcthumbs[i], 'src', index, field)
        refthumb = create_thumbnail(refthumbs[i], 'ref', index, field)
        cls = ""
        conf = 0

        source = Source(cls=cls, conf=conf, validity=validity,
                        detidx=index, pos=pos, field=field,
                        diffthumb=diffthumb, detthumb=detthumb,
                        srcthumb=srcthumb, refthumb=refthumb)
        source.save()

        store_features(features[i], source)
        #sources.append(source)


def process(filename, field="", target="", nmax=None):
    logger = logging.getLogger(__name__)
    logger.info("Reading FITS file")
    with fits.open(filename) as hdulist:
        if not target:
            if hdulist[0].data is None and hdulist[0].data.ndim != 2:
                header = hdulist['image'].header
            else:
                header = hdulist[0].header
            target = header.get('target')
            if not target:
                target = header.get('object')
                if not target:
                    logger.warning("no target found for %s. Ignored", filename)
                    return
        if not field:
            regex = re.search('\_(?P<field>\d{4})\_', filename)
            if regex:
                field = regex.group('field')
            else:
                logger.warning("no field found for %s. Ignored", filename)
                return

        field = store_field(hdulist, field, target, nmax)
        store_sources(hdulist, field, nmax)
        logger.info("All done")


def setup_logger(level):
    logger = logging.getLogger(__name__)
    level = ['ERROR', 'WARNING', 'INFO', 'DEBUG'][level]
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s: %(message)s')
    handler.setFormatter(formatter)
    handler.setLevel(level)
    logger.addHandler(handler)
    logger.setLevel('DEBUG')
    logger.info("hello")


class Command(BaseCommand):
    help = 'Store the observation FITS file(s) in the database'

    def add_arguments(self, parser):
        parser.add_argument('files', nargs='+')
        parser.add_argument('--field', help="Field name. Obtained from "
                            "file name if not given")
        parser.add_argument('--target', help="Target name. Obtained from "
                            "header if not given")
        parser.add_argument('--max-sources', type=int,
                            help="Maximum number of sources to store for an "
                            "input file, ordered by 'validity'")

    def handle(self, *args, **options):
        setup_logger(options.get('verbosity', 0))
        for filename in options['files']:
            process(filename, field=options['field'], target=options['target'],
                    nmax=options['max_sources'])

        self.stdout.write(self.style.SUCCESS('Successfully stored observations'))
